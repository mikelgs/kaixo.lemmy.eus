---
title: "Netiketa"
weight: 20
draft: false
---

{{< expand "Aurkibidea" "..." >}}
- [Arauak](#arauak)
- [Moderazioa](#moderazioa)
{{< /expand >}}

{{< hint warning >}}
**Abisua**\
Lemmy.eus webgunean parte hartzen baduzu, ondoko baldintzak onartzen dituzu. Kontuan izan edozein unetan aldatu daitezkeela eta zure ardura dela berauok aldizka berrikustea.
{{< /hint >}}

## Arauak

1. **Begirunea**. Guztiontzako atsegina, segurua eta erosoa den giro bat sortzeko asmoa dugu, edozein dela ere erabiltzailearen esperientzia, genero-identitate eta -adierazpena, sexu-orientazioa, ezgaitasuna, itxura pertsonala, gorputzaren tamaina, arraza, etnia, adina, erlijioa, nazionalitatea edo antzeko beste ezaugarriren bat. Izan atsegin eta adeitsua. Ez dago zakarra edo desatsegina izateko beharrik. Ez erabili generoz oso markatutako ezizenak edo beste erabiltzaile-izen batzuk, horiek denontzako giro adiskidetsu, seguru eta atsegin bat sortzea galarazi badezakete. Eztabaidatzea, eszeptikoa edo kritikoa izatea aberasgarria izan daiteke, baina modu eraikitzailean egiten bada bakarrik.

1. **Erantzukizuna**. Webguneko erabiltzaileak, bidaltzen dituzten eduki guztien erantzuleak dira.

1. **Irainik ez**. Inor iraindu, degradatu edo jazartzen baduzu, interakzioa galaraziko dizugu, jarrera hori ez delako ongi etorria. Bereziki, ez dugu onartzen gizarteak baztertutako taldeetako banakoak baztertzen dituen portaera. Ez ezkutatu anonimatuan beste inor iraintzeko. Salaketaren bat egiten baduzu inoren kontra, saiatu berau frogatzen, eta, beti, sinatu mezu hori zeure izenean.

1. **Letra larririk ez**. Ez idatzi LETRA LARRIZ. Honela idaztea ez dago ondo ikusia Interneten, garrasika zabiltzala adierazten baitu.

1. **Izenburu adierazgarriak**. Ez bidali izenbururik gabeko mezurik. Desatsegina izaten da beste partaideentzat horrelako mezua jasotzea.

1. **Zaratarik ez**. Eutsi komunitate bakoitzaren egitasmo nagusiari, eutsi mezu-hariaren gaiari. Noiz edo noiz digresioak edo *off-topic* mezuak onartzen eta eskertzen badira ere, ez da komeni komunitatearen helburu nagusitik, mezu-hariaren gai nagusitik, behin eta berriro alde egitea. Komunitateetako partaide askorentzat gai nagusia alboratzen duten mezuak "zarata" dira; komunikazioari lagundu beharrean, komunikazioa oztopatzen duten mezuak.

1. **Euskara**. Webgune hau euskararen eta euskal kulturaren komunitatearentzat sortutakoa da eta euskaraz bizi da. Erabiltzaileren batek beste hizkuntzaren bat erabili nahi izanez gero, izan dadila noizean behingo zerbait eta arrazoi ulergarri batengatik; adibidez, partekatu nahi duen materiala edo informazioa erdaraz bakarrik dagoelako eta komunitate horren gehiengoak interesgarria deritzolako. Arau honen salbuespen bakarra, hizkuntza bat ikasteko edo munduko kultura bat ezagutzeko espresuki sortutako komunitateak dira. Bertan, erdaraz aritzea onartzen da.

1. **Formatu irekiak**. Hobetsi formatu irekia, formatu unibertsala. Erabili formatu irekia fitxategi erantsiak bidaltzeko. Formatu pribatiboa duten zenbait fitxategi software pribatiboarekin bakarrik zabaldu daitezke. Ez duzu zertan behartu beste erabiltzaileak software hori erosi edota erabiltzera. Formatu irekia duten fitxategiak, software libreko aplikazioek onartuko dute. Eta software pribatuko aplikazioek ere onartu beharko lukete, adostutako estandarrak direlako.

1. **Jazarpena**. Debekatuta dago beste erabiltzaile batzuk zelatatzea edo jazartzea. Ez du axola nor den, komunitateko kide batek jazarri edo deseroso sentiarazi duela sentitzen badu, mesedez, jar zaitez harremanetan kanaletako batekin edo Lemmyren moderazio-taldeko edozeinekin. Erabiltzaile erregularra edo etorri berria bazara, komunitate hau zuretzat leku segurua izatea eta zuretzako babesa edukitzea axola zaigu. Debekatuta dago beste erabiltzaile batzuek zerbitzua erabiltzea oztopatzea, zerbitzuaren errendimendua degradatzea edota beste erabiltzaile batzuk jazarpenera edo aipatutako ekintzak burutzera bultzatzea.

1. **Debekatutako edukiak**:

   1. **Eduki ilegalak**. Espainiar edo Frantziar estatuetako legediaren arabera legez kanpokoak diren edukiak ez dira onartzen: egile eskubideen urraketak, haur pornografia...

   1. **Besteen datu pertsonalak**. Debekatuta dago baimen espliziturik gabe pertsona baten datu pertsonalak argitaratzea.

   1. **Eduki automatikoak**. Webgune hau gizakiek gizakientzat egina da. Edonola automatizatutako edukiak bidaltzea (script bidez, bot bidez, adimen artifiziala erabiliz...) debekatuta dago. Etorkizunean, salbuespen bakanen bat egiteko aukera egon liteke, beti ere aurretiaz instantziaren administratzaileen baimena lortuz gero.

   1. **Publizitatea**. Iragarkiak eta bestelako eduki komertzialak ez dira onartzen.

   1. **Bortizkeria**. Indarkeria grafikoa edo gorea ez dira onartzen, artisten irudikapenak barne.

   1. **Intolerantzia**. Fanatismoa edo gorroto-diskurtsoa bezala sailkatu daitezkeen eduki hauek espresuki debekatuak daude: arrazakeria, sexismoa, xenofobia, genero edo sexu-orientazioaren araberako diskiminazioa (adibidez, [LGBTIQ+](https://eu.wikipedia.org/wiki/LGBT) pertsonen aurkako jarrera edo [TERF](https://eu.wikipedia.org/wiki/TERF) jarrera), ideologia nazionalsozialista bultzatzea, holokaustoaren ukazioa, Nazien sinbologia, gutxiengoen aurkako gorroto diskurtsoa. Hauetako edozein diskurtsoren defentsa ere debekatua dago.

1. **Eduki hunkigarriak (NSFW)**. NSFW siglek *"Not Safe For Work"* [lanerako desegokia] esan nahi dute. Adingabeentzat egokiak ez diren edukiak, hizkera desegokia, erotismoa, pornoa... onartzen dira, beti ere webguneko beste arauak betetzen badituzte eta bidalketek "Eduki hunkigarriak (NSFW)" marka badute.

## Moderazioa

Moderazioari dagokionez, komunitateetako moderatzaileek eta instantziaren administratzaileek hartzen dituzten neurri ohikoenak ondokoak dira:
1. **Bidalketa bat blokeatu**, denbora baterako edo betirako.

2. **Erabiltzaile bat debekatu** (baneatu), denbora baterako edo betirako, komunitate batean edo webgune osoan.

Komunitateak moderazio aldetik ezin dira utzita egon. Administratzaileek komunitate bat moderazio gabe ez geratzeko, dauden moderatzaileak aldatu ditzakete eta inor interesatuta agertzen ez bada, komunitatea itxi liteke.

Webgunean egindako moderazio ekintza guztien erregistroa publikoa da eta hemen ikus daiteke: https://lemmy.eus/modlog

Moderatzaileek eta instantziaren administratzaileek, askatasuna dute une eta kasu bakoitzean egokiak deritzoten neurriak hartzeko, legeak ezarritako mugen barruan.
