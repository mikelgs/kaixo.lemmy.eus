---
title: "App"
weight: 50
draft: false
---

{{< expand "Aurkibidea" "..." >}}
- [Android](#android)
- [iOS](#ios)
- [Bestelakoa](#bestelakoa)
{{< /expand >}}

Lemmy instantziak, *smartphone* eta *tablet* gailuetan zure gogoko nabigatzaile librearekin bisitatu ditzakezu. Instalatutako app baten bitartez edo web aplikazio baten bitartez erabiltzeko, software ofizialen zerrenda kontsulta dezakezu: https://join.lemmy.ml/apps/

## Android

Googlen sistema eragile honetan lemmy.eus app bezala erabiltzeko, ondoko urratsak jarrai ditzakezu:

1\. **Firefox** nabigatzailearekin [lemmy.eus](https://lemmy.eus/) webgunea bisitatu eta nabigazio-barran 3 puntutxoen ikonoa sakatu:
 > ![lemmy.eus Firefoxekin](/android-firefox-app-1.png)

2\. Laster-menuaren beheko aldean agertzen den *"Gehitu hasierako pantailan"* aukera sakatu:
> ![Gehitu hasierako pantailan](/android-firefox-app-2.png)

3\. Zure smartphoneko hasierako pantailan ikono berri bat gehituko da:
> ![Ikonoa hasierako pantailan](/android-firefox-app-3.png)

4\. Ikonoa sakatu eta zuzenean lemmy.eus webgunera helduko zara. Bertan saioa hastea gomendatzen dizugu dena prest izateko.

## iOS

Applen sistema eragile honetan lemmy.eus app bezala erabiltzeko, ondoko urratsak jarrai ditzakezu:

1\. **Safari** nabigatzailearekin [lemmy.eus](https://lemmy.eus/) webgunea bisitatu eta nabigazio-barran menua zabaltzeko ikonoa sakatu:
 > ![lemmy.eus Safarin](/ios-app-1.png)

2\. Laster-menuan agertzen den *"Añadir a pantalla de inicio"* aukera sakatu:
> ![Gehitu hasierako pantailan](/ios-app-2.png)

3\. Zure iPhoneko hasierako pantailan ikono berri bat gehituko da:
> ![Ikonoa hasierako pantailan](/ios-app-3.png)

## Bestelakoa

Sistema eragile libre batzuk AOSP (*Android Open Source Project*) delakoan oinarrituta daude (*LineageOS*, */e/OS*...) eta beste batzuk *Debian*, *Ubuntu* edo beste sistema eragile libre batzuetan (*PureOS*, *Ligrem*, *PlasmaMobile*, *PinePhone*...). Edonola ere, **Firefox** nabigatzailearen erabilera antzekoa da gehienetan eta, beraz, hauetan jarraitu behar duzun prozedura [Androiden antzekoa](/edukiak/app/#android) izango da.
