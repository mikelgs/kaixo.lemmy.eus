---
title: "Ohiko galderak"
weight: 30
draft: false
---

{{< expand "Aurkibidea" "..." >}}
**Hasierako galderak**
- [Nola parte hartu dezaket?](#nola-parte-hartu-dezaket)
- [Nola has naiteke idazten?](#nola-has-naiteke-idazten)
- [Komunitateetara harpidetu behar dut?](#komunitateetara-harpidetu-behar-dut)
- [Nola sortzen da komunitate bat?](#nola-sortzen-da-komunitate-bat)
- [Nola idazten da bidalketa bat?](#nola-idazten-da-bidalketa-bat)
- [Nola idazten da iruzkin bat?](#nola-idazten-da-iruzkin-bat)
- [Zer dira bozkak?](#zer-dira-bozkak)
- [Nola egiten da nire izenaren ondoan iruditxo bat agertzeko?](#nola-egiten-da-nire-izenaren-ondoan-iruditxo-bat-agertzeko)
- [Smartphone edo sakelako telefono adimendunetik parte hartu dezaket?](#smartphone-edo-sakelako-telefono-adimendunetik-parte-hartu-dezaket)

**Webguneari buruzkoak**
- [Nola egiten da letra lodiak eta etzanak erabiltzeko? Eta bidalketen barruan eta iruzkinetan webguneak estekatzeko?](#nola-egiten-da-letra-lodiak-eta-etzanak-erabiltzeko-eta-bidalketen-barruan-eta-iruzkinetan-webguneak-estekatzeko)
- [Zer dira komunitateak?](#zer-dira-komunitateak)
- [Zein komunitate daude?](#zein-komunitate-daude)
- [Nortzuk dira moderatzaileak?](#nortzuk-dira-moderatzaileak)
- [Nola bilakatu naiteke moderatzaile?](#nola-bilakatu-naiteke-moderatzaile)
- [Nola bidaltzen zaio mezu zuzen bat erabiltzaile bati?](#nola-bidaltzen-zaio-mezu-zuzen-bat-erabiltzaile-bati)
- [Nola jasotzen ditut mezu zuzenak?](#nola-jasotzen-ditut-mezu-zuzenak)
- [Gero eta jakinarazpen gehiago ditut, ez dira joaten. Nola kendu dezaket agertzen zaidan zenbakitxo hori?](#gero-eta-jakinarazpen-gehiago-ditut-ez-dira-joaten-nola-kendu-dezaket-agertzen-zaidan-zenbakitxo-hori)
- [Instantziak orria zergatik dago hutsik? Zertarako da?](#instantziak-orria-zergatik-dago-hutsik-zertarako-da)

**Moderazioari buruzkoak**
- [Nola moderatzen dira bidalketak eta iruzkinak?](#nola-moderatzen-dira-bidalketak-eta-iruzkinak)
- [Zer dago baimenduta eta zer ez?](#zer-dago-baimenduta-eta-zer-ez)
- [Zer dira "eduki hunkigarriak" edo "NSFW" delako hori?](#zer-dira-eduki-hunkigarriak-edo-nsfw-delako-hori)
- [Ez daukat argi gauza bat bidali/idatzi dezakedan ala ez, edo NSFW jarrita bai ala horrela ere ez. Zer egingo dut?](#ez-daukat-argi-gauza-bat-bidaliidatzi-dezakedan-ala-ez-edo-nsfw-jarrita-bai-ala-horrela-ere-ez-zer-egingo-dut)
- [Norbaitek egokia ez dela iruditzen zaidan zerbait bidali edo esan du. Zer egin dezaket?](#norbaitek-egokia-ez-dela-iruditzen-zaidan-zerbait-bidali-edo-esan-du-zer-egin-dezaket)

**Lemmy delakoari buruzkoak**
- [Nondik dator "Lemmy" izena?](#nondik-dator-lemmy-izena)
- [Zer da Lemmy webgune bat?](#zer-da-lemmy-webgune-bat)
- [Zer da fedibertsoa?](#zer-da-fedibertsoa)
- [Lemmy.eus norekin dago federatuta?](#lemmyeus-norekin-dago-federatuta)
- [Non lor dezaket Lemmyri buruzko informazio gehiago?](#non-lor-dezaket-lemmyri-buruzko-informazio-gehiago)

**Galdera gehiago**
- [Zer egin dezaket nire zalantza hemen argitzen ez bada?](#zer-egin-dezaket-nire-zalantza-hemen-argitzen-ez-bada)

{{< /expand >}}

## Hasierako galderak

#### Nola parte hartu dezaket?

> Edukia irakurtzeko webgunean sartu eta kito, ez duzu konturik behar honetarako. Edukia sortzeko (bidalketak bozkatu eta sortu, iruzkinak bozkatu eta sortu...) lehenik eta behin izena eman behar duzu, hau da webgunean erregistratu behar duzu. Irakurri [lehen urratsak](/edukiak/hasi/) lemmy.eus-en izena emateko.

#### Nola has naiteke idazten?

> [Lehen urratsak](/edukiak/hasi/) jarraitu badituzu, orain erabiltzaile bat izango duzu. Idatzi ahal izateko, lehenik eta behin, erabiltzaile horrekin saioa hasi behar duzu. Hori egindakoan, hainbat gauza egin ahal izango duzu: albisteak partekatu, galderak egin, besteek idatzitakoa bozkatu, iruzkinak idatzi...

#### Komunitateetara harpidetu behar dut?

> Ez daukazu zertan. Zure gustuko komunitateetara harpidetzen bazara, lemmy.eus bisitatzean *"Harpidetuta"* aukeratu eta zure gustuko gaiak iragazi ahalko dituzu, baina bestela ere, *"Guztiak"* aukeratuta eduki guztiak ikusten jarraituko duzu.

#### Nola sortzen da komunitate bat?

> Goiko barran (edo menuan, pantaila estuetan) dagoen *"Sortu komunitatea"* sakatu behar duzu. Agertuko zaizun formularioan, hainbat eremu eskainiko zaizkizu:
> - **Izena**: eremu honetan komunitatearen betirako izena idatzi behar duzu. Izen hau ezin da aldatu, komunitatearen esteketarako izena izango baita. Izen honek letra xehez eta hutsunerik gabe izan behar du, baina azpimarrak (_) onartzen dira; gehienez 20 hizki eduki ditzake.
> - **Bistaratzeko izena**: zure komunitatearen izen atsegina da hau, komunitatearen izenburu gisa agertuko dena. Eremu hau edonoiz aldatu daiteke!
> - **Ikonoa**: zure komunitatearen iruditxoa, aukerakoa.
> - **Banerra**: zure komunitatearen atzealdeko irudi zabala, orriaren goikaldean egon ohi dena; aukerakoa.
> - **Alboko barra**: zure komunitatearen alboko barrako testua zehaztu dezakezu hemen: azalpena, arauak, informazio gehiago... nahi duzun formatua eman diezaiokezu!
> - **Kategoria**: eremu honetan zure komunitatea zein gairen inguruan jorratuko den zehaztu behar duzu; oraingoz kategoria hauen izenak ingelesez soilik ikusi daitezke.
> - **Eduki hunkigarria (NSFW)**: aukera hau markatuta zure komunitateko bidalketa **guztiak** hunkigarri gisa markatuko dira automatikoki; honela, norbaitek bere ezarpenetan hunkigarritzat markatutako edukiak ez ikustea erabaki badu, ez du ikusiko. Edozein momentutan aukera hau desmarkatuz gero, eduki guztiei automatikoki kenduko zaie eduki hunkigarrien marka.

#### Nola idazten da bidalketa bat?

> Hasteko, goiko barran (edo menuan, pantaila estuetan) dagoen *"Sortu bidalketa"* sakatu behar duzu. Agertuko zaizun formularioan, hainbat eremu eskainiko zaizkizu:
> - **URL**: eremu honetan Interneteko helbide bat (URL bat) jar  dezakezu. Bidalketa prest dagoenean, norbaitek Izenburuan sakatzean helbide hori bisitatuko du. URL eremuaren eskuinaldean, azpian, irudiaren ikonoa sakatuz, URL baten ordez, irudi bat igotzeko aukera duzu. Honela, zure bidalketak, beste webgune bat estekatu ordez, zuk igotako irudia izango du. Edonola ere, eremu hau hautazkoa da; URLrik edo Irudirik sartzen ez baduzu, bidalketa berdin argitaratu dezakezu. Kasu honetan, bidalketaren gakoa (beste erabiltzaileei interesgarri egingo zaien hori) Gorputza eremuan egotea esperokoa da: galderaren bat, zure hausnarketaren bat...
> - **Izenburua**: zure bidalketaren gaia, titulua edo izenburua. Derrigorrezko eremua da. Saiatu esaldi labur bat idazten, hitz gutxirekin bidalketa hori zertaz doan azalduz.
> - **Gorputza**: zure bidalketaren edukia, testua, irudiak, estekak... nahi duzuna!
> - **Komunitatea**: eremu honetan zein komunitatetan argitaratu nahi duzun zehazten  duzu. Derrigorrezko eremua da. Ez duzu komunitate horretan harpidetuta egon beharrik.
> - **Eduki hunkigarria (NSFW)**: aukera hau markatuta zure bidalketa hunkigarri gisa markatuko da; honela, norbaitek bere ezarpenetan hunkigarritzat markatutako edukiak ez ikustea erabaki badu, ez du ikusiko.

#### Nola idazten da iruzkin bat?

> Bidalketen azpian jendeak iruzkintxoak gehitzen ditu. Zuk ere hori egin nahi baduzu, bidalketa batean sartu eta bertan dagoen koadroan zure galdera, iritzia edo dena delakoa idatzi dezakezu.

#### Zer dira bozkak?

> Bozkek bidalketak ikusarazten laguntzen dute, sailkapenean gorago agertu daitezen. Alde edo aurka bozkatu dezakezu, erabiltzaile bakoitzak behin. Iruzkinak ere bozkatu ditzakezu.

#### Nola egiten da nire izenaren ondoan iruditxo bat agertzeko?

> Zure erabiltzaileari avatar bat esleitu diezaiokezu. Hori nola egin [lehen urratsak](/edukiak/hasi/#zure-ezarpenak-doitu) orrian azaltzen da.

#### Smartphone edo sakelako telefono adimendunetik parte hartu dezaket?

> Bai, hori nola egin [app](/edukiak/app/) orrian azaltzen da.

***

## Webguneari buruzkoak

#### Nola egiten da letra lodiak eta etzanak erabiltzeko? Eta bidalketen barruan eta iruzkinetan webguneak estekatzeko?

> Lemmyk Markdown deitzen den sintaxia erabiltzen du. Edukiei formatua emateko eskaintzen zaizkizun ikonoak erabil ditzakezu, baina nahi izanez gero zuzenean sintaxi hori erabiliz idatzi dezakezu. Laguntza behar izanez gero, galdera ikurra duen ikonoa sakatu eta [Markdown gidatxoa](https://lemmy.eus/docs/about_guide.html#markdown-guide) irekiko zaizu.

#### Zer dira komunitateak?

> Komunitateak gai bat lantzen duten gune birtualak dira. Lemmy.eus-en edozeri buruz hitz egin daitekeenez, komunitateetan interes berdintsuak dituzten pertsonak biltzen dira. Pertsona bakoitzak komunitate batean baino gehiagotan parte hartu ohi du, baina normalean ez denetan.

#### Zein komunitate daude?

> Hasieratik dauden komunitateak [!lemmyeus_iragarpenak](https://lemmy.eus/c/lemmyeus_iragarpenak) eta [!lemmyeus_laguntza](https://lemmy.eus/c/lemmyeus_laguntza) dira. Beste komunitate guztiak erabiltzaileek sortuak dira. Zuk ere komunitate bat sortu dezakezu! Lemmy.eus-en oraintxe bertan existitzen diren komunitate guztien zerrenda hemen ikus dezakezu: https://lemmy.eus/communities

#### Nortzuk dira moderatzaileak?

> Komunitate bat sortzen duen erabiltzailea, automatikoki, bertako moderatzailea bihurtzen da. Lehen moderatzaile horrek, nahi badu, moderatzaile gehiago izendatu ditzake. Komunitate bakoitzak azalpen batzuk ditu eta bertan Lemmy.eus-eko arau orokorrez gain, beste arau, ohitura edo gomendio batzuk existitu daitezke. Kontsultatu lauki hori moderatzaileak nortzuk diren jakiteko eta komunitate horrek nola jarduten duen ezagutzeko. Komunitate bakoitzaren moderazioa komunitatean bertan egiten bada ere, Lemmy.eus-eko administratzialeek eskua sartu lezakete arau orokorrak betetzen ez direla ikusiz gero.

#### Nola bilakatu naiteke moderatzaile?

> Komunitate bateko moderatzaile izan nahi baduzu, komunitatean bertan proposatu dezakezu edo komunitate horretako moderatzaileren bati zure laguntza eskaini diezaiokezu berari mezu zuzen bat bidaliz. Dagoeneko existitzen diren moderatzaileen eskumena da moderazio lana nortzuk egiten duten erabakitzea, beti ere Lemmy.eus-en arau orokorrak betez egiten badute.

#### Nola bidaltzen zaio mezu zuzen bat erabiltzaile bati?

> Erabiltzaile horren profilean sartu behar duzu bere izena sakatuz eta, bertan, *"Bidali mezua"* sakatuz. Zure erabiltzailearen ezarpenetan Matrix zehaztu baduzu, erabiltzaile batzuen profilean *"Bidali mezu segurua"* aukera ikusiko duzu, pertsona horrekin Matrix bidez modu zifratuan komunikatzeko.

#### Nola jasotzen ditut mezu zuzenak?

> Mezu zuzenak jakinarazpenen orrian ikusten dira. Sakatu goiko eskuin aldean (edo menuan, pantaila estuetan) agertzen den kanpaia mezu zuzenak eta bestelako jakinarazpenak irakurtzeko.

#### Gero eta jakinarazpen gehiago ditut, ez dira joaten. Nola kendu dezaket agertzen zaidan zenbakitxo hori?

> Jakinarazpenak ikustea ez da nahikoa, eskuz markatu behar dituzu irakurritako gisa. Horretarako, sakatu jakinarazpen bakoitzaren azpian agertzen den lehen ikurra (tik txiki bat).

#### Instantziak orria zergatik dago hutsik? Zertarako da?

> Lemmy erabiltzen duen instantzia edo webgune bakoitza, beste batzuekin federatu edo lotu daiteke. Lemmy.eus oraingoz ez dago beste inorekin federatuta eta horregatik dago [instantziak](https://lemmy.eus/instances) orria hutsik. Baina primeran legoke beste Lemmy instantzia batekin edo batzuekin federatzea eta [euskal fedibertsoa](/#fedibertsoa) indartzea!

***

## Moderazioari buruzkoak

#### Nola moderatzen dira bidalketak eta iruzkinak?

> Eduki bat moderatu ahal izateko, eduki hori dagoen komunitatean moderatzailea izan behar duzu, bestela ezin duzu moderatu. Moderatzailea bazara, bidalketa edo iruzkin batean sartu eta azpian dauzkan hiru puntutxoak sakatu; bertan agertuko zaizkizu moderatzeko aukerak.

#### Zer dago baimenduta eta zer ez?

> Lemmy.eus-ek [arau orokorrak](/edukiak/netiketa/#arauak) ditu. Honetaz gain, gerta daiteke komunitate batek arau, ohitura edo gomendio gehigarriak (eta arau orokorrekiko osagarriak) izatea. Azken hauek komunitatean sartzean, lauki batean ikusgai egongo dira, baldin eta baimen aldetik zerbait berezia zehaztuta badago (normalean ez).

#### Zer dira "eduki hunkigarriak" edo "NSFW" delako hori?

> *"Eduki hunkigarriak"* argitaratzen diren bidalketei egileek jartzen dieten marka bat da; komunitate bati marka hau jarri dakioke, bertako bidalketa guztiek marka hau izan dezaten automatikoki. Bidalketa bat sortzen zaudenean eduki horiek beste erabiltzaile batzuei hunkigarria suertatu dakiekeela pentsatzen baduzu, arrazoia dena dela ere, marka hori beti gehitzea gomendatzen dizugu (ziur ez bazaude ere, badaezpada). Honela, beraien erabiltzaileko ezarpenetan eduki hunkigarriak ez ikustea aukeratu dutenek, bidalketa hori ez dute ikusiko.

#### Ez daukat argi gauza bat bidali/idatzi dezakedan ala ez, edo NSFW jarrita bai ala horrela ere ez. Zer egingo dut?

> Irakurri lemmy.eus-eko [arau orokorrak](/edukiak/netiketa/#arauak) berriro. Bertan egin ezin denik esaten ez bada, hasiera batean egin dezakezu. Eduki hunkigarria (NSFW) marka zein eduki-motak behar duen ere bertan azaltzen da. Hala ere, zalantzak sortzea ulergarria eta esperokoa da. Horregatik, arauen interpretazioan edonolako zalantzarik baduzu, bidali zure galdera [!lemmyeus_laguntza](https://lemmy.eus/c/lemmyeus_laguntza) komunitatera eta denon artean laguntzen eta argitzen saiatuko gara. Nahiago baduzu, lemitarren txat komunitatean ([Matrixen](https://matrix.to/#/#lemmyeus_komunitatea:sindominio.net) eta [Telegramen](https://t.me/lemmyeus_komunitatea) dago) edo [info@lemmy.eus](mailto:info@lemmy.eus) helbidera eposta mezu bat bidaliz.

#### Norbaitek egokia ez dela iruditzen zaidan zerbait bidali edo esan du. Zer egin dezaket?

> Iruzkin bat jarri zure iritzia modu eraikitzailean emanez eta moderatzaileari eduki hori egokia den ala ez galdetuz. Moderatzaileen esku geratzen da hasiera batean edukien egokitasuna erabakitzea eta moderazio lana egitea.

***

## Lemmy delakoari buruzkoak

#### Nondik dator "Lemmy" izena?

> Lemmy Motörhead taldeko sortzailea, abeslaria eta burua izan zen musikariarengandik dator. Ez genuen guk hautatutako izena, baizik eta Lemmy softwarea sortu eta mantentzen dutenek.

#### Zer da Lemmy webgune bat?

> Lemmy software baten izena da, librea eta federatua. Lemitarrak, lemmy.eus bezalako instantziak erabiltzen dituztenak dira (instantziak, Lemmy softwarea darabilten webguneak dira). Instantzia bakoitzak bere izaera eta arauak ditu eta euskaraz aritu daiteke hau bezala edo beste edozein hizkuntzatan. Lemmy software librea hemen dago eskuragarri (nahi baduzu, librea zara berau kopiatzeko, moldatzeko eta software desberdin bat argitaratzeko): https://github.com/LemmyNet/lemmy

#### Zer da fedibertsoa?

> Hain zuzen ere, orri honetako [Fedibertsoa](/#fedibertsoa) atalean, galdera horri erantzuten saiatzen gara.

#### Lemmy.eus norekin dago federatuta?

> Oraingoz inorekin ez. Euskarazko beste Lemmy instantziarik agertzen denean, beraiekin federatzea espero dugu. Lemmy instantzia bat sortzekotan bazaude eta laguntzarik behar baduzu, jar zaitez [gurekin harremanetan](/#kontaktatu).

#### Non lor dezaket Lemmyri buruzko informazio gehiago?

> Webgune hau informatzen jarraitzeko leku aproposa izan daiteke: https://join.lemmy.ml/

***

## Galdera gehiago

#### Zer egin dezaket nire zalantza hemen argitzen ez bada?

> [!lemmyeus_laguntza](https://lemmy.eus/c/lemmyeus_laguntza) komunitatean galdetu dezakezu (idatzi aurretik irakurri ea beste norbaitek dagoeneko gauza berdina galdetu duen). Nahiago baduzu, lemitarren txat komunitatean galdetu dezakezu (horretarako Matrix edo Telegram behar duzu). Beste aukera bat, administratzaileekin zuzenean kontaktatzea da, [info@lemmy.eus](mailto:info@lemmy.eus) helbidera idatziz (horretarako eposta kontu bat behar duzu).
